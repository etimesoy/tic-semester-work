def main():
    books_indices = map(int, input("Введите последовательность индексов позиций, через пробел: ").split())
    bu_index = int(input("Введите номер строки из преобразования Б-У: "))

    books = list(range(256))
    letters = []
    for index in books_indices:
        letter = chr(books[index])
        letters.append(letter)
        books = [books[index]] + books[0:index] + books[index+1:]
    print(f"Последовательность символов перед кодированием стопкой книг: {''.join(letters)}")

    bu_words = sorted(letters)
    for _ in range(len(letters) - 1):
        for i in range(len(letters)):
            bu_words[i] = letters[i] + bu_words[i]
        bu_words.sort()
    initial_letters = bu_words[bu_index]
    print(f"Исходная последовательность символов: {initial_letters}")


if __name__ == "__main__":
    main()
